﻿using ClassPlay;
using System;

namespace ClassPlay
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*            Employee employee1 = new Employee();*/
            /*            Employee employee2 = new Employee("Jane", "Doe", 2);*/

            // initialize objects using two different constructors
            Employee employee1 = new Employee();
            Employee employee2 = new Employee("Pursalane", "Faye", 2);
            // without obj initializer
            /*            employee.FirstName = "John";
                        employee.LastName = "Doe";
                        employee.EmployeeId = 1;
                        employee.Year = 2020;
                        employee.JobTitle = "Sales Representative";
                        employee.Salary = 52000;*/

            // with obj initializer
            // Using the object initializer syntax
            Employee employee3 = new Employee
            {
                FirstName = "Dana",
                LastName = "Scully",
                EmployeeId = 3,
                Year = "2021",
                JobTitle = "Engineer",
                Salary = 64000
            };



            employee1.DisplayEmployee();
            employee2.DisplayEmployee();
            employee3.DisplayEmployee();

            employee3.PromoteEmployee();

        /*            DisplayEmployee(employee1);
                    DisplayEmployee(employee2);
                    DisplayEmployee(employee3);*/
        }

/*        static void DisplayEmployee(Employee employee)
        {
            Console.WriteLine("Full Name: {0} {1}", employee.FirstName, employee.LastName);
            Console.WriteLine("Employee ID: {0}", employee.EmployeeId);
            Console.WriteLine("With company since: {0}", employee.Year);
            Console.WriteLine("Job Title: {0}", employee.JobTitle);
            Console.WriteLine("Salary: {0}", employee.Salary);
            Console.WriteLine();

        }*/
    }
}