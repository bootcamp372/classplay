﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClassPlay
{
    public class Employee
    { // Properties
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public int EmployeeId { get; set; } = 0;
        public string Year { get; set; } = "";
        public string JobTitle { get; set; } = "";
        public string Dept { get; set; } = "";
        public decimal Salary { get; set; } = 0;

        // Constructors

        public Employee(string firstName, string lastName, int employeeId, string year, string jobTitle, string dept, decimal salary)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.EmployeeId = employeeId;
            this.Year = year;
            this.JobTitle = jobTitle;
            this.Dept = dept;
            this.Salary = salary;
        }

        public Employee(string firstName, string lastName, int employeeId)
        {
            DateTime now = DateTime.Today;

            {
                FirstName = firstName;
                LastName = lastName;
                EmployeeId = employeeId;
            }

/*            this.FirstName = firstName;
            this.LastName = lastName;
            this.EmployeeId = employeeId;*/
            this.Year = now.ToString("yyyy");
            this.JobTitle = "New Hire";
            this.Dept = "TBD";
            this.Salary = 31200;
        }

        // use the parameterized constructor for
        // property assignment
        public Employee() : this("Jane", "Doe", 1)
        {
        }

        public void DisplayEmployee()
        {
    
            Console.WriteLine("Full Name: {0} {1}", FirstName,LastName);
            Console.WriteLine("Employee ID: {0}", EmployeeId);
            Console.WriteLine("With company since: {0}", Year);
            Console.WriteLine("Job Title: {0}", JobTitle);
            Console.WriteLine("Salary: {0:C}", Salary);
            Console.WriteLine();

        }

        public void PromoteEmployee()
        {
            //increases salary by 10%
            this.Salary = Salary + (Salary * 0.10M);
            // change job title
            this.JobTitle = "Senior " + JobTitle;
            DisplayEmployee();

        }
    }
}
